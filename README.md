## Project Name
Secondhand Web API Testing

## Project Description
Performed API testing on website Secondhand which consists of making Collections, Requests, Headers configurations, Body configurations, Test configurations on Postman which are made based on API documentation and exported into JSON format. Also created an API testing report using Newman and save it in .html format.
